<?php

use yii\db\Migration;

/**
 * Handles the creation for table `projects_table`.
 */
class m180310_134705_create_projects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('projects_table', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'user_id' => $this->integer(),
            'cost' => $this->string()->notNull(),
            'date_start' => $this->dateTime()->notNull(),
            'date_of_delivery' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime()->defaultValue('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime(),
        ]);

      $this->addForeignKey('projects_table', '{{%projects_table}}', 'user_id', '{{%users_table}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('projects_table');
    }
}
