<?php

use yii\db\Migration;
use common\models\User;
/**
 * Handles the creation for table `users_table`.
 */
class m180310_121239_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users_table', [
            'id' => $this->primaryKey(),
            'fullname' => $this->string(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->insert('users_table', [
            'fullname' => 'admin',
            'username' => 'admin',  
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'email' => 'admin',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users_table');
    }
}
