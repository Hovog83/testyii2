<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects_table".
 *
 * @property integer $id
 * @property string $title
 * @property integer $user_id
 * @property string $cost
 * @property string $date_start
 * @property string $date_of_delivery
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects_table';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'cost', 'date_start', 'date_of_delivery'], 'required'],
            [['user_id'], 'integer'],
            [['date_start', 'date_of_delivery', 'created_at', 'updated_at'], 'safe'],
            [['title', 'cost'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'user_id' => 'User ID',
            'cost' => 'Cost',
            'date_start' => 'Date Start',
            'date_of_delivery' => 'Date Of Delivery',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
